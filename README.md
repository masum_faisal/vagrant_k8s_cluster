# vagrant-k8s-cluster

Kubernetes cluster automation via Vagrant

# Prerequisites

To create VMs with vagrant, you need to install:
- Vagrant (This project is tested on Vagrant 2.4.0)
- Virtualbox (This project is tested on Virtualbox 6.1.48)


# k8s-cluster

vagrant_k8s_cluster_master (ip: 192.168.56.10)

vagrant_k8s_cluster_worker1 (ip: 192.168.56.21)

vagrant_k8s_cluster_worker2 (ip: 192.168.56.22)

# Scripts

Prerequisite installation: install-kubernetes-dependencies.sh

Master node cinfiguration: configure-master-node.sh

Worker node configuration: configure-worker-nodes.sh

k8s Dashboard and Metric server configuration: k8s-dashboard.sh

# Usage

To create VMs and bootstrap your kubernetes cluster

`vagrant up`

After creation of VMs is complete ssh into master and check kubernetes cluster status

`vagrant ssh master`

`kubectl get nodes`

To check your kubernetes cluster, you can create an nginx deployment and expose it (from port 30080) with

`kubectl apply -f /vagrant/nginx-deployment.yml && kubectl apply -f /vagrant/nginx-service.yml`

After deployment you can check your page with 

`curl http://<worker-ip>:30080`


# k8s dashboard and metric server setup

To setup the k8s Dashboard and Metric server, execute the k8s-dashboard.sh script from /vagrant directory under vagrant_k8s_cluster_master (ip: 192.168.56.10).

`$ /vagrant/k8s-dashboard.sh`

# Note

If you face "/bin/bash^M: bad interpreter" issue while executing the k8s-dashboard.sh script, then convert k8s-dashboard.sh file from DOS line endings by executing below command under vagrant_k8s_cluster_master (ip: 192.168.56.10):

`$ dos2unix /vagrant/k8s-dashboard.sh`

Now execute below command under vagrant_k8s_cluster_master (ip: 192.168.56.10):

`$ /vagrant/k8s-dashboard.sh`

